# Simple Dapp (Calculator)

This is a sample app to use as a template for [ETHBerlin](https://ethberlin.com/) hackathon.

Please clone/fork this app (or just download a bundle if you prefer so) and start hacking.
Devops workflow will start working if you publish your code on any gitlab instance ([public one](https://gitlab.com) is free to use even for privatly-developed projects).
Please **don't forget to change** the `PROJECT_DOMAIN` environment variable to your actual domain in the top lines of your `.gitlab-ci.yml`, otherwise your deployments won't work as expected.

Your friendly DevOps mentors (available via https://ethberlin-helpq.herokuapp.com/) will be happy to provide you with:

 - Kubernetes resources (including publicly-facing IP addresses, to actually deploy your app);
 - Ethereum testnet ether so you can test your up in more production-like environment;
 - DNS records if you don't have any suitable domains at hand;
 - faster build runners (obviously publicly-available shared runners on gitlab.com are constantly under havy load);
 - lots of tips on running more complicated flows for development situations.


## Recommended flow

Do your development in feature branches; then just open a Merge Request to the `master` branch in your Gitlab instance.

This will automatically run all the tests on your project (Javascript-related with `npm test`, Solidity-related with `truffle test` and contracts migration test run with `truffle migrate` run against a Parity node running on an individual dev chain).

After those test will pass, it will compile your sources, build a docker container (fully available afterwards, more details can be found under the "Registry" sidebar link of your repo) — and deploy it to the subdomain of your `$PROJECT_DOMAIN` in an independent environment. Those "review" environments will be kept up-to-date with your feature branch, and will be automatically destroyed after your Merge Request is merged (or rejected and closed).

After you're OK with that Merge Request and it has been merged, the whole test suite will be run once again, on a fresh code in `master` (to avoid any unexpected artifacts messing with your code during automated git merge). After that your latest master will be deployed to the staging.$PROJECT_DOMAIN host, and can be later promoted to the actual `production` environment with a click of a button in your "Environments" sidebar item (that "play" button dropdown in a row with your `staging` environment will contain the desired "Deploy to production" item). 

Please also note how that "Environments" section has some metrics for your deployments automatically collected by Prometheus instance in your Kubernetes cluster, and there's even a "open console" button right there, so you can remotely debug your deployed containers right from the Gitlab Web UI — isn't that neat?


### Useful commands for local development

```bash
truffle migrate --network ganache

truffle test

npm test

npm run-script build
```

### Where to tweak things

 - `.gitlab-ci.yml` — the actual file containing CI setup, build steps and actual commands run in the CI. It should be pretty thoroughly documented.
 - `deployment.template.yml` — Kubernetes _deployment_ which actually describes how to set up your project on Kubernetes. Talk to hackathon mentors if you want to tweak that somehow, but you don't know how!
 - `truffle.js` — this is where you set up your Truffe suite to communicate with desired RPC node. It should be enough to just set `PROVIDER_ENDPOINT` and `ETH_ACCOUNT` environment variables from your Gitlab CI settings; no need to tweak that JS file manually for most setups.
 - `Dockerfile` — pretty self-evident; tweak it, if you need something more than just a plain old nginx serving your static _webpacked_ files to the world.


## License

This repo is MIT-licensed. This means that you're free to use it in your work in any way (_"use, copy, modify, merge, publish, distribute, sublicense, and/or sell"_) provided you include the original copyright notice in all copies of your software.
