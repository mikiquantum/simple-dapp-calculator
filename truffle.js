let WalletProvider = require("truffle-wallet-provider");

let endpoint = process.env.PROVIDER_ENDPOINT || "http://127.0.0.1:8545";
let b64account = process.env.ETH_ACCOUNT || "";
let pass = process.env.ETH_PASSWORD || "";

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    dev: {
      provider: () => {
        let keystore = Buffer.from(b64account, 'base64').toString();
        let wallet = require('ethereumjs-wallet').fromV3(keystore, pass);
        return new WalletProvider(wallet, endpoint)
      },
      network_id: "17",
      gas: 4712388
    },
    kovan: {
      provider: () => {
        let keystore = Buffer.from(b64account, 'base64').toString();
        let wallet = require('ethereumjs-wallet').fromV3(keystore, pass);
        return new WalletProvider(wallet, endpoint)
      },
      network_id: "42",
      gas: 4712388
    },
    ganache: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      gas: 4712388
    }
  }
};
