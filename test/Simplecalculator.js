const SimpleCalculator = artifacts.require("./SimpleCalculator.sol");

contract("SimpleCalculator", accounts => {
  it("...should calculate some value", async () => {
    const simpleCalculatorInstance = await SimpleCalculator.deployed();

    let v0 = 10;
    let v1 = 100;

    let value = await simpleCalculatorInstance.addition(v0, v1, { from: accounts[0] });

    assert.equal(value, v0+v1, "value was not calculated properly");
  });
});
